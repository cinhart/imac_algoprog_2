#include <QApplication>
#include <time.h>

#include "tp2.h"


MainWindow* w=nullptr;

void recursivQuickSort(Array& toSort, int size)
{
	// stop statement = condition + return (return stop the function even if it does not return anything)
    if (size!=0){
	
        Array& pivotArray= w->newArray(size);
        Array& lowerArray = w->newArray(size);
        Array& greaterArray= w->newArray(size);
        //Array& Sorted= w->newArray(size);
        int lowerSize = 0, greaterSize = 0, pivotSize=0; // effectives sizes

	// split

        int pivot=toSort[size/2];
        for(int i=0;i<size;i++){
            if(toSort[i]<pivot){
                lowerArray.insert(lowerSize,toSort[i]);
                lowerSize+=1;
            }
            if(toSort[i]>pivot){
                greaterArray.insert(greaterSize,toSort[i]);
                greaterSize+=1;
            }
            if(toSort[i]==pivot){
                pivotArray.insert(pivotSize,toSort[i]);
                pivotSize+=1;
            }
        }

	// recursiv sort of lowerArray and greaterArray
        recursivQuickSort(lowerArray, lowerSize);
        recursivQuickSort(greaterArray, greaterSize);

        /* merge
            int sortedSize=0;
            for(int i=0;i<lowerSize;i++){
                Sorted[sortedSize]=lowerArray[i];
                sortedSize+=1;
            }
            for(int i=0;i<pivotSize;i++){
                Sorted[sortedSize]=pivotArray[i];
                sortedSize+=1;
            }
            for(int i=0;i<greaterSize;i++){
                Sorted[sortedSize]=greaterArray[i];
                sortedSize+=1;
            }*/

        //merge
        for(int i=0;i<lowerSize;i++){
            toSort[i]=lowerArray[i];
        }
        for(int i=0;i<pivotSize;i++){
            toSort[lowerSize+i]=pivotArray[i];
        }
        for(int i=0;i<greaterSize;i++){
            toSort[lowerSize+pivotSize+i]=greaterArray[i];
        }
    }

}

void quickSort(Array& toSort){
	recursivQuickSort(toSort, toSort.size());
}


int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	uint elementCount=20;
	MainWindow::instruction_duration = 50;
    w = new TestMainWindow(quickSort);
	w->show();

	return a.exec();
}
