#include "tp4.h"
#include "mainwindow.h"

#include <QApplication>
#include <time.h>
#include <stdio.h>

MainWindow* w = nullptr;
using std::size_t;
using std::string;

int Heap::leftChild(int nodeIndex)
{
    int i=nodeIndex;
    return i*2+1;
}

int Heap::rightChild(int nodeIndex)
{
    int i=nodeIndex;
    return i*2+2;
}

void Heap::insertHeapNode(int heapSize, int value)
{
	// use (*this)[i] or this->get(i) to get a value at index i
	int i = heapSize;
    this->get(i)=value;
    while(i>0 && this->get(i)>this->get((i-1)/2)){
        swap(i,(i-1)/2);
        i=(i-1)/2;
    }
}

void Heap::heapify(int heapSize, int nodeIndex)
{
	int i_max = nodeIndex; 
    int largest = i_max;
    if(this->leftChild(nodeIndex)<heapSize){
        if(this->get(i_max)<this->get(this->leftChild(nodeIndex))){
            largest=this->leftChild(i_max);
        }
    }
    if(this->rightChild(nodeIndex)<heapSize){
        if(this->get(largest)<this->get(this->rightChild(nodeIndex))){
            largest=this->rightChild(i_max);
        }
    }
    if(largest!=i_max){
        swap(largest,i_max);
        heapify(heapSize,largest);
    }
}

void Heap::buildHeap(Array& numbers)
{
    for (int i=0;i<numbers.size();i++){
        insertHeapNode(i,numbers[i]);
    }
}

void Heap::heapSort()
{
    for (int i=this->size()-1;i>=0;i--){
        swap(0,i);
        heapify(i,0);
    }
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    MainWindow::instruction_duration = 50;
    w = new HeapWindow();
	w->show();

	return a.exec();
}
