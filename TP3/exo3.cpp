#include "mainwindow.h"
#include "tp3.h"
#include <QApplication>
#include <time.h>
#include <stack>
#include <queue>

MainWindow* w = nullptr;
using std::size_t;

struct BinarySearchTree : public BinaryTree
{    
    Node* left;
    Node* right;
    int value;

    void initNode(int value)
    {
        // init initial node without children
        this->value=value;
        this->left=0;
        this->right=0;
    }

	void insertNumber(int value) {
        // create a new node and insert it in right or left child
        Node* newNode = createNode(value);
        if(value<this->value){
            if(this->left==0){
                //this->left->insertNumber(value);
                this->left=newNode;
            }
        }
        if(value>this->value){
            if(this->right==0){
                //this->right->insertNumber(value);
                this->right=newNode;
            }
        }
    }

	uint height() const	{
        // should return the maximum height between left child and
        // right child +1 for itself. If there is no child, return
        // just 1

        while (this->left!=0){
            return left->height();
        }
        if(this->left==0){
            return 1;
        }
        while (this->right!=0){
            return right->height();
        }
        if(this->right==0){
            return 1;
        }
        uint h = std::max(right->height(), left->height())+1;
        return h;
    }

	uint nodesCount() const {
        // should return the sum of nodes within left child and
        // right child +1 for itself. If there is no child, return
        // just 1

        if(this->left==0 && this->right==0){
            return 1;
        }

        else{
            int count=0;
            if(this->left!=0){
                count+=this->left->nodesCount();
            }
            if(this->right!=0){
                count+=this->right->nodesCount();
            }
            return count+1;
        }

	}

	bool isLeaf() const {
        // return True if the node is a leaf (it has no children)

        if(nodesCount()==1){
            return true;
        }
        return false;

	}

	void allLeaves(Node* leaves[], uint& leavesCount) {
        // fill leaves array with all leaves of this tree

        if(isLeaf()){
            leaves[leavesCount]=this;
            leavesCount+=1;
        }

        if(this->right!=0){
           this->right->allLeaves(leaves, leavesCount);
        }
        if(this->left!=0){
            this->left->allLeaves(leaves, leavesCount);
        }

	}

	void inorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with inorder travel
	}

	void preorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with preorder travel
	}

	void postorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with postorder travel
	}

	Node* find(int value) {
        // find the node containing value
		return nullptr;
	}

    void reset()
    {
        if (left != NULL)
            delete left;
        if (right != NULL)
            delete right;
        left = right = NULL;
    }

    BinarySearchTree(int value) : BinaryTree(value) {initNode(value);}
    ~BinarySearchTree() {}
    int get_value() const {return value;}
    Node* get_left_child() const {return left;}
    Node* get_right_child() const {return right;}
};

Node* createNode(int value) {
    return new BinarySearchTree(value);
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 200;
    w = new BinarySearchTreeWindow<BinarySearchTree>();
	w->show();

	return a.exec();
}
