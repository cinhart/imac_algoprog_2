#include "tp3.h"
#include <QApplication>
#include <time.h>

MainWindow* w = nullptr;
using std::size_t;

int binarySearch(Array& array, int toSearch)
{
    int start = 0;
    int end = array.size()-1;

    while(end-start>1){

        int dicho=(start+end)/2;

        if(array[dicho]<toSearch){
            start=dicho;
        }

        if(array[dicho]>toSearch){
            end=dicho;
        }

        if(array[dicho]==toSearch){
            return dicho;
        }

    }
	return -1;
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 500;
	w = new BinarySearchWindow(binarySearch);
	w->show();

	return a.exec();
}
