#include "tp1.h"
#include <QApplication>
#include <time.h>

int isMandelbrot(Point z, int n, Point point){

    if(n>0){
        Point z2;
        z2.x = (z.x)*(z.x)-(z.y)*(z.y)+point.x;
        z2.y = 2*(z.x)*(z.y)+point.y;
        float mod;
        mod=sqrt((z2.x)*(z2.x)+(z2.y)*(z2.y));

        if(mod>2){
            return n;
        }
        else{

            return isMandelbrot(z2, n-1, point);

        }

    }
    else {
        return true;
    }

}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow* w = new MandelbrotWindow(isMandelbrot);
    w->show();

    a.exec();
}



